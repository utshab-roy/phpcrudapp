<?php
session_start();
require_once("config.php");

//necessary array for the messages
$output = array();
$output['validation'] = 1;
$output['success_messages'] = array();
$output['validation_messages'] = array();
$messages  = array();
$validation_messages  = array();
$success_messages  = array();


$firstname = $lastname = $email = $password = $password_confirm = '';

$email_error        = '';
$password_error     = '';
$general_error      = '';
$first_name_error = $last_name_error = '';
$password_confirm_error = '';

$formValid = true;
$fieldEmailValid = true;
$fieldPasswordValid = true;


//retrieving data from the form
if (isset($_POST['first_name'])){
    $firstname = $_POST['first_name'];
    if(empty($firstname)){
        $validation_messages['first_name'] = 'First name is empty';
        $output['validation'] = 0;

    }elseif (!preg_match("/^([a-zA-Z']+)$/",$firstname)){
        $validation_messages['first_name'] = 'First name is not valid, name can not have digit';
        $output['validation'] = 0;
    }
}

if (isset($_POST['last_name'])){
    $lastname = $_POST['last_name'];
    if(empty($lastname)){
        $validation_messages['last_name'] =  'Last name is empty';
        $output['validation'] = 0;

    }elseif (!preg_match("/^([a-zA-Z']+)$/",$lastname)){
        $validation_messages['last_name'] =  'Last name is not valid, can not have digit in name';
        $output['validation'] = 0;
    }
}

if (isset($_POST['email'])){
    $email = $_POST['email'];
    if(empty($email)){
        $validation_messages['email'] =  'Email is empty';
        $output['validation'] = 0;

    }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $validation_messages['email'] =  'Invalid email';
        $output['validation'] = 0;
    }

    if (intval($output['validation']) == 1 && (email_exists($email) == true)){
        $validation_messages['email'] =  'Sorry, email already exists';
        $output['validation'] = 0;
    }
}

if (isset($_POST['psw'])){
    $password = $_POST['psw'];
    if(empty($password)){
        $validation_messages['psw'] =  'Password can not be empty';
        $output['validation'] = 0;
    }
}

if (isset($_POST['psw_confirm'])){
    $password_confirm = $_POST['psw_confirm'];
    if(empty($password_confirm)){
        $validation_messages['psw_confirm'] =  'Confirm password can not be empty';
        $output['validation'] = 0;
    }
}

//checking whether the password matches
if (intval($output['validation']) == 1) {
    if($password !== $password_confirm){
        $validation_messages['psw_confirm'] =  'Confirm password didn\'t match';
        $output['validation'] = 0;
    }
}

$output['validation_messages'] = $validation_messages;

if(intval($output['validation']) == 1){
    //if form validation passed
    $success_messages =  sign_up($firstname, $lastname, $email, $password);
    $output['success_messages'] = $success_messages;
    echo json_encode($output);
}
else{
    //form is not valid
    echo json_encode($output);
}
die();