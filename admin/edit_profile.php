<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

require_once("config_admin.php");
include 'header.php';

$firstname = $lastname = $email = $address1 = $address2 = $city = $state = $country = $zip_code = '';
$first_name_error = $last_name_error = $email_error = $address1_error = $address2_error = $city_error = $state_error = $country_error = $zip_code_error = '';
$formValid = true;

$user_id = $_SESSION['id'];

$sql_for_user_data = "SELECT * FROM users WHERE id= $user_id";

$result = $conn_oop->query($sql_for_user_data);
$address = array();
if ($result->num_rows == 1){
    $row = $result->fetch_assoc();

    $firstname = $row['firstname'];
    $lastname= $row['lastname'];
    $email = $row['email'];

    if (!($row['info'] === "")) :
        $address = unserialize($row['info']);
//    var_dump($address);
    endif;
}


if(isset($_POST['edit']) && intval($_POST['edit']) == 1){

    if (isset($_POST['first_name'])){
        $firstname = $_POST['first_name'];
        if(empty($firstname)){
            $formValid = false;
            $first_name_error = 'First name is empty';
        }elseif (!preg_match("/^([a-zA-Z']+)$/",$firstname)){
            $formValid = false;
            $first_name_error = 'First name is not valid';
        }
    }

    if (isset($_POST['last_name'])){
        $lastname = $_POST['last_name'];
        if(empty($lastname)){
            $formValid = false;
            $last_name_error = 'Last name is empty';
        }elseif (!preg_match("/^([a-zA-Z']+)$/",$lastname)){
            $formValid = false;
            $last_name_error = 'Last name is not valid';
        }
    }

    if (isset($_POST['email'])){
        $email = $_POST['email'];
        if(empty($email)){
            $formValid = false;
            $email_error = 'Email is empty';
        }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $formValid = false;
            $email_error = 'Email is not valid';
        }
    }

    $address = array();

    if (isset($_POST['address1'])){
        $address1 = $_POST['address1'];
        $address['address1'] = $address1;
        if(empty($address1)) {
            $address1 = "Not given";
        }
    }

    if (isset($_POST['address2'])){
        $address2 = $_POST['address2'];
        $address['address2'] = $address2;

        if(empty($address2)) {
            $address2 = "Not given";
        }
    }

    if (isset($_POST['city'])){
        $city = $_POST['city'];
        $address['city'] = $city;
        if(empty($city)) {
            $city = "Not given";
        }
    }

    if (isset($_POST['state'])){
        $state = $_POST['state'];
        $address['state'] = $state;
        if(empty($state)) {
            $state = "Not given";
        }
    }
    if (isset($_POST['country'])){
        $country = $_POST['country'];
        $address['country'] = $country;
        if(empty($country)) {
            $country = "Not given";
        }
    }
    if (isset($_POST['zip_code'])){
        $zip_code = $_POST['zip_code'];
        $address['zip_code'] = $zip_code;
        if(empty($zip_code)) {
            $zip_code = "Not given";
        }
    }

    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    echo "this is the target file: " . $target_file . "<br/>";
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    echo "this is the imageFileType: " . $imageFileType . "<br/>";
// Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
// Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
// Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "uploads/" . $firstname.".".$imageFileType)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
    $address = serialize($address);

    if ($formValid){
        //inserting data into users table
        $sql = "UPDATE  users SET firstname='$firstname', lastname='$lastname', email='$email', info='$address' WHERE id='$user_id'";
//        $sql = "UPDATE category SET name='$title' WHERE id='$category_id'";
        if ($conn_oop->query($sql) === TRUE) {
            echo "Update data into info column.";
            header('location: user_profile.php');
        }else{
            echo $conn_oop->error;
        }
    }
}
else{
    $address1 = isset($address['address1'])? $address['address1']: '';
    $address2 = isset($address['address2'])? $address['address2']: '';
    $city = isset($address['city'])? $address['city']: '';
    $state = isset($address['state'])? $address['state']: '';
    $country = isset($address['country'])? $address['country']: '';
    $zip_code = isset($address['zip_code'])? $address['zip_code']: '';
}
?>

<body>

<div class="container">
    <div class="row">

        <form action="edit_profile.php" method="post" id="user_edit_profile" enctype="multipart/form-data">

            <div class="form-group" >
                <label for="firstname"><b>Firstname</b></label>
                <input class="form-control" type="text" placeholder="Enter firstname" name="first_name" id="first_name" value="<?php echo $firstname; ?>" />
                <p><?php echo $first_name_error; ?></p>

                <label for="lastname"><b>Lastname</b></label>
                <input class="form-control" type="text" placeholder="Enter lastname" name="last_name" id="last_name" value="<?php echo $lastname; ?>" />
                <p><?php echo $last_name_error; ?></p>

                <label for="email"><b>Email</b></label>
                <input class="form-control" type="text" placeholder="Enter Email" name="email" id="email" value="<?php echo $email; ?>" />
                <p><?php echo $email_error; ?></p>

                <!--            ***********************************-->

                <label for="address1"><b>address line-1</b></label>
                <input class="form-control" type="text" placeholder="Enter address" name="address1" id="address1" value="<?php echo $address1; ?>" />
                <p><?php echo $address1_error; ?></p>

                <label for="address2"><b>address line-2</b></label>
                <input class="form-control" type="text" placeholder="Enter address" name="address2" id="address2" value="<?php echo $address2; ?>" />
                <p><?php echo $address2_error; ?></p>

                <label for="city"><b>City</b></label>
                <input class="form-control" type="text" placeholder="Enter city" name="city" value="<?php echo $city; ?>" />
                <p><?php echo $city_error; ?></p>

                <label for="state"><b>State</b></label>
                <input class="form-control" type="text" placeholder="Enter state" name="state" id="state" value="<?php echo $state; ?>" />
                <p><?php echo $state_error; ?></p>
<!--                ***************************************-->
                <?php
                //this function will run a query and get all the countries name from the database
                $result = get_all_country_name();
                ?>
                <label for="country"><b>Country select</b></label>
                <select class="form-control" id="exampleSelect1" name="country">
                    <option value="" <?php if ($country == '') echo 'selected'; ?> >Choose your country...</option>
                    <?php
                    foreach ($result as $row):?>
                        <option value="<?php echo $row['country_name'] ?>" <?php if ($country == $row['country_name']) echo 'selected'; ?> ><?php echo $row['country_name'] ?></option>
                    <?php
                    endforeach; ?>
                </select>
<!--                ****************************************-->

                <label for="zip_code"><b>Zip-code</b></label>
                <input class="form-control" type="text" placeholder="Enter zip-code" name="zip_code" id="zip_code" value="<?php echo $zip_code; ?>" />
                <p><?php echo $zip_code_error; ?></p>

                <label for="upload_profile_pic"><b>Upload profile pic</b></label>
                <input class="form-control" type="file"  name="fileToUpload" id="fileToUpload" />

            </div>

<!--            ***********************************-->

            <button name="edit" value="1" type="submit" class="btn btn-primary">Confirm</button>
            <br/>
            <br/>
        </form>

    </div>

</div>

<script src="../js/main.js"></script>
<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>

</body>
</html>
