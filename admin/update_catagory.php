<?php
require_once("config_admin.php");
include 'header.php';

$title = '';
$formValid = true;

$category_id = isset($_GET['id'])? intval($_GET['id']): 0;

if (isset($_POST['submit']) && intval($_POST['submit'] == 1)){

    $category_id = isset($_POST['id'])? intval($_POST['id']): 0;

    if (isset($_POST['title'])){

        $title = $_POST['title'];

        if(empty($title)){
            $formValid = false;
            $email_error = 'Title is empty';
        }
    }

    if ($formValid){
        //updating the category name
        $row = update_category($title, $category_id);
    }

}else{
    if (isset($_GET['type']) && intval($_GET['id']) > 0) {
        //editing category name by id
        $category_id = intval($_GET['id']);
        //show the category that we want to change the name
        $row = show_category_by_id($category_id);

    }

}
?>

<body>
<form action="update_catagory.php" method="post">
    <div class="form-group col-lg-4">
        <label for="title"><h2>Category name</h2></label>
        <input type="text" class="form-control" name="title" autofocus="autofocus" value="<?php echo $row['name']?>">

        <input type="hidden" name="id" value="<?php echo $category_id ?>">
        </br>
        <button class="btn btn-primary" type="submit" name="submit" value="1">Submit</button>
    </div>
</form>

<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>