<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

require_once("config_admin.php");
include 'header.php';

$firstname = $lastname = $email = $address1 = $address2 = $city = $state = $country = $zip_code = '';
$first_name_error = $last_name_error = $email_error = $address1_error = $address2_error = $city_error = $state_error = $country_error = $zip_code_error = '';
$formValid = true;

$user_id = $_SESSION['id'];

$sql_for_user_data = "SELECT * FROM users WHERE id= $user_id";

$result = $conn_oop->query($sql_for_user_data);
$address = array();
if ($result->num_rows == 1){
    $row = $result->fetch_assoc();

    $firstname = $row['firstname'];
    $lastname= $row['lastname'];
    $email = $row['email'];

    if (!($row['info'] === "")) :
        $address = unserialize($row['info']);
//    var_dump($address);
    endif;
}


if(isset($_POST['edit']) && intval($_POST['edit']) == 1){

    if (isset($_POST['first_name'])){
        $firstname = $_POST['first_name'];
        if(empty($firstname)){
            $formValid = false;
            $first_name_error = 'First name is empty';
        }elseif (!preg_match("/^([a-zA-Z']+)$/",$firstname)){
            $formValid = false;
            $first_name_error = 'First name is not valid';
        }
    }

    if (isset($_POST['last_name'])){
        $lastname = $_POST['last_name'];
        if(empty($lastname)){
            $formValid = false;
            $last_name_error = 'Last name is empty';
        }elseif (!preg_match("/^([a-zA-Z']+)$/",$lastname)){
            $formValid = false;
            $last_name_error = 'Last name is not valid';
        }
    }

    if (isset($_POST['email'])){
        $email = $_POST['email'];
        if(empty($email)){
            $formValid = false;
            $email_error = 'Email is empty';
        }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $formValid = false;
            $email_error = 'Email is not valid';
        }
    }

    $address = array();

    if (isset($_POST['address1'])){
        $address1 = $_POST['address1'];
        $address['address1'] = $address1;
        if(empty($address1)) {
            $address1 = "Not given";
        }
    }

    if (isset($_POST['address2'])){
        $address2 = $_POST['address2'];
        $address['address2'] = $address2;

        if(empty($address2)) {
            $address2 = "Not given";
        }
    }

    if (isset($_POST['city'])){
        $city = $_POST['city'];
        $address['city'] = $city;
        if(empty($city)) {
            $city = "Not given";
        }
    }

    if (isset($_POST['state'])){
        $state = $_POST['state'];
        $address['state'] = $state;
        if(empty($state)) {
            $state = "Not given";
        }
    }
    if (isset($_POST['country'])){
        $country = $_POST['country'];
        $address['country'] = $country;
        if(empty($country)) {
            $country = "Not given";
        }
    }
    if (isset($_POST['zip_code'])){
        $zip_code = $_POST['zip_code'];
        $address['zip_code'] = $zip_code;
        if(empty($zip_code)) {
            $zip_code = "Not given";
        }
    }

    $address = serialize($address);

    if ($formValid){
        //inserting data into users table
        $sql = "UPDATE  users SET firstname='$firstname', lastname='$lastname', email='$email', info='$address' WHERE id='$user_id'";
//        $sql = "UPDATE category SET name='$title' WHERE id='$category_id'";
        if ($conn_oop->query($sql) === TRUE) {
            echo "Update data into info column.";
            header('location: user_profile.php');
        }else{
            echo $conn_oop->error;
        }
    }
}
else{
    $address1 = isset($address['address1'])? $address['address1']: '';
    $address2 = isset($address['address2'])? $address['address2']: '';
    $city = isset($address['city'])? $address['city']: '';
    $state = isset($address['state'])? $address['state']: '';
    $country = isset($address['country'])? $address['country']: '';
    $zip_code = isset($address['zip_code'])? $address['zip_code']: '';
}
?>
<style>
    .prev, .next { background-color:#b0232a; padding:5px 10px; color:#fff; text-decoration:none;}
    .prev:hover, .next:hover { background-color:#000; text-decoration:none;}
    .prev { float:left;}
    .next { float:right;}
    #steps { list-style:none; width:100%; overflow:hidden; margin:0px; padding:0px;}
    #steps li {font-size:24px; float:left; padding:10px; color:#b0b1b3;}
    #steps li span {font-size:11px; display:block;}
    #steps li.current { color:#000;}
    /*#makeWizard { background-color:#b0232a; color:#fff; padding:5px 10px; text-decoration:none; font-size:18px;}*/
    /*#makeWizard:hover { background-color:#000;}*/
</style>

<body>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
<script type="text/javascript" src="../js/formToWizard.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#user_edit_profile").formToWizard({ submitButton: 'submit_profile' })
    });
</script>


<div class="container">
    <div class="row">

        <form action="edit_profile.php" method="post" id="user_edit_profile">
            <fieldset>
                <legend>Basic information</legend>
                <label for="firstname"><b>Firstname</b></label>
                <input class="form-control" type="text" placeholder="Enter firstname" name="first_name" id="first_name" value="<?php echo $firstname; ?>" />
                <p><?php echo $first_name_error; ?></p>

                <label for="lastname"><b>Lastname</b></label>
                <input class="form-control" type="text" placeholder="Enter lastname" name="last_name" id="last_name" value="<?php echo $lastname; ?>" />
                <p><?php echo $last_name_error; ?></p>

                <label for="email"><b>Email</b></label>
                <input class="form-control" type="text" placeholder="Enter Email" name="email" id="email" value="<?php echo $email; ?>" />
                <p><?php echo $email_error; ?></p>
            </fieldset>

            <fieldset>
                <legend>Personal information</legend>
                <label for="address1"><b>address line-1</b></label>
                <input class="form-control" type="text" placeholder="Enter address" name="address1" id="address1" value="<?php echo $address1; ?>" />
                <p><?php echo $address1_error; ?></p>

                <label for="address2"><b>address line-2</b></label>
                <input class="form-control" type="text" placeholder="Enter address" name="address2" id="address2" value="<?php echo $address2; ?>" />
                <p><?php echo $address2_error; ?></p>
            </fieldset>

            <fieldset>
                <legend>Global information</legend>
                <label for="city"><b>City</b></label>
                <input class="form-control" type="text" placeholder="Enter city" name="city" value="<?php echo $city; ?>" />
                <p><?php echo $city_error; ?></p>

                <label for="state"><b>State</b></label>
                <input class="form-control" type="text" placeholder="Enter state" name="state" id="state" value="<?php echo $state; ?>" />
                <p><?php echo $state_error; ?></p>

                <?php
                $sql_for_country = "SELECT * FROM apps_countries";
                $result = $conn_oop->query($sql_for_country);
                $count = $result->num_rows;
                $result->fetch_assoc();
                ?>
                <label for="country"><b>Country select</b></label>
                <!--                <select multiple="true" class="form-control chosen-select" id="exampleSelect1" name="country">-->
                <select data-placeholder="Choose a country..."  class="chosen form-control" name="country">
                    <!--                    <option value="" --><?php //if ($country == '') echo 'selected'; ?><!-- >Choose your country...</option>-->
                    <?php foreach ($result as $row): ?>
                        <option value="<?php echo $row['country_name'] ?>" <?php if ($country == $row['country_name']) echo 'selected'; ?> ><?php echo $row['country_name'] ?></option>
                    <?php endforeach; ?>
                </select>
                <!--                ****************************************-->
                <br/>
                <label for="zip_code"><b>Zip-code</b></label>
                <input class="form-control" type="number" placeholder="Enter zip-code" name="zip_code" id="zip_code" value="<?php echo $zip_code; ?>" />
                <p><?php echo $zip_code_error; ?></p>
            </fieldset>

            <div class="form-group" >

            </div>

            <!--            ***********************************-->

            <button name="edit" value="1" type="submit" class="btn btn-primary" id="submit_profile">Confirm</button>
            <br/>
            <br/>
        </form>

    </div>

</div>

<!--chosen include-->
<script src="../lib/chosen/chosen.jquery.js"></script>
<!--Select2 lib-->
<script src="../lib/select2/js/select2.min.js"></script>

<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    //    using chosen for drop-down
    $(".chosen").data("placeholder","Select Frameworks...").chosen({width: "100%"});
</script>
</body>
</html>
