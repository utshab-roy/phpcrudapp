<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

include 'header.php';
include 'paginator.class.php';
require_once("config_admin.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";


if (isset($_GET['action']) && (intval($_GET['action']) == 1)) {
    $type = isset($_GET['type']) ? $_GET['type'] : 'none';

    switch ($type){
        case 'delete':
            //getting the category_id
            $category_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
            //category delete by id
            delete_category($category_id);
            break;

        default:
    }
}
?>

<body>

<div class="container">
    <h1>Category</h1>
    <h4>List of category:</h4>
    <ul class="list-group">
        <?php
        //*****************************************
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", "$username", "$password");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $num_rows = $conn->query('SELECT COUNT(*) FROM category')->fetchColumn();
//        var_dump($num_rows);
        $pages = new Paginator($num_rows,9,array(5,3,6,9,12,25,50,100,250,'All'));
//        echo $pages->display_pages();

        $stmt = $conn->prepare('SELECT * FROM category ORDER BY category.id ASC LIMIT :start,:end');
        $stmt->bindParam(':start', $pages->limit_start, PDO::PARAM_INT);
        $stmt->bindParam(':end', $pages->limit_end, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        //*****************************************


            foreach($result as $row) : ?>

                <li class="list-group-item"><?php echo $row['name']?>
                    <a class="btn btn-danger btn-sm float-right"
                       href="category.php?id=<?php echo $row['id']; ?>&type=delete&action=1">Delete</a>
                    <a class="btn btn-primary btn-sm float-right"
                       href="update_catagory.php?id=<?php echo $row['id']; ?>&type=edit&action=1">Edit</a>
                </li>
            <?php endforeach;
//        echo "<span style='text-align: center;' class= 'container' >".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";
        ?>
        <span class= 'container text-lg-center ' ><?php echo$pages->display_jump_menu(); ?> <?php echo $pages->display_items_per_page();?></span>

    </ul>
    <div class= 'container text-lg-center '><?php echo $pages->display_pages();?></div>

    <a href="category_new.php" class="btn float-lg-left btn-primary mb-5">Add category</a>
</div>
<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>
