
jQuery(document).ready(function ($) {
    //for the default comment output we need to call just this class


    var $signup_form    = $("#sign_up_form");
    var $login_form     = $("#login_form");

    // js validation for login-form
    $login_form.validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            psw: "required"
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            psw: "Enter your password"
        }
    });


    //js for sign-up-form
    $signup_form.validate({
        rules: {
            first_name  : "required",
            last_name   : "required",
            email:{
                required: true,
                email: true
            },
            psw:{
                required    : true,
                minlength   : 5
            },
            psw_confirm:{
                required    : true,
                minlength   : 5,
                equalTo     :"#psw"
            }
        },
        messages:{
            first_name  : "Enter your first name",
            last_name   : "Enter last name",
            email:{
                required: "You must give your email",
                email   : "Please give a valid email"
            },
            psw:{
                required    : "Give your PASSWORD",
                minlength   : "Password must be at lest 5 char long"
            },
            psw_confirm:{
                required    : "Re-enter your password",
                minlength   : "Password must be at lest 5 char long",
                equalTo     : "Password has to be equal"
            }
        }
    });

    $signup_form.on('submit', function (event) {
        event.preventDefault();

        if($signup_form.valid()){
            console.log('form submitted, sending ajax request');

            var $form_data_serialized = $("#sign_up_form").serialize();

            $.ajax({
                type: "POST",
                url: "registration_ajax.php",
                data: $form_data_serialized,
                dataType: 'json',
                beforeSend: function () {

                },
                cache: false,
                success: function (data) {
                    console.log(data);

                    if(data.validation == 0){
                        //highlight the error fields and show error messages
                        var $validation_messages = data.validation_messages;
                        // console.log($validation_messages);


                        $.each($validation_messages, function (index, value) {
                            var $error_field = '<label id="'+index+'-error" class="error" for="'+index+'">'+value+'</label>';
                            if($('#'+index+'-error').length > 0){
                                $('#'+index+'-error').html(value).show();
                            }
                            else{
                                $('#'+index).after($error_field);
                            }
                        });
                    }
                    else{
                        //highlight the success fields and show the messages
                        var $success_messages = data.success_messages;

                        $.each($success_messages, function (index, value) {
                            $('#form_messages').html('<div class="alert alert-success" role="alert">'+value+'</div>').show();
                        });
                        $('#form_messages').append('<a type="button" class="btn btn-primary btn-lg" href="login.php">Login</a>').show();
                        $('#sign_up_form').remove();
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            }); // end of ajax method
        } //end of if $signup_form.valid()


    });

    $login_form.on('submit', function (event) {
        event.preventDefault();
        if ($login_form.valid()) {
            console.log('Login form submitted, sending ajax request');

            var $login_form_data_serialized = $login_form.serialize();
            $.ajax({
                type: "POST",
                url: "login_ajax.php",//need tobe changed
                data: $login_form_data_serialized,
                dataType: 'json',
                beforeSend: function () {

                },
                cache: false,
                success: function (data) {
                    console.log(data);

                    if (data.validation == 0) {
                        //highlight the error fields and show error messages
                        var $validation_messages = data.validation_messages;
                        // console.log($validation_messages);


                        $.each($validation_messages, function (index, value) {
                            var $error_field = '<label id="' + index + '-error" class="error" for="' + index + '">' + value + '</label>';
                            if ($('#' + index + '-error').length > 0) {
                                $('#' + index + '-error').html(value).show();
                            }
                            else {
                                $('#' + index).after($error_field);
                            }
                        });
                    }
                    else {

                        //highlight the success fields and show the messages
                        var $success_messages = data.success_messages;

                        $.each($success_messages, function (index, value) {
                            if (index === 'login'){
                                $('#form_messages').html('<div class="alert alert-success" role="alert">'+value+'</div>').show();
                                $('#login_form').remove();
                                $('#form_messages').append('<a type="button" class="btn btn-primary btn-lg" href="admin/index.php">Admin Panel</a>').show();
                            }else{
                                $('#form_messages').html('<div class="alert alert-danger" role="alert">'+value+'</div>').show();
                            }
                        });
                        $('#sign_up_form').remove();
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            }); // end of ajax method

        } //end of if $login_form.valid()
    });

    $("#email").on('focus', function () {
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        if (first_name && last_name && !this.value) { // this condition prevent to reset the previous value
            this.value = first_name + "." + last_name + "@gmail.com";
        }
    });


}); // end of document.ready function

