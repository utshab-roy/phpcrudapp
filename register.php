<?php
session_start();
require_once("config.php");
include 'header.php';
?>

<!DOCTYPE html>
<html>

<head>
    <title>Sign up</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
<div class="container">
    <h2>Sign Up</h2>
    <p>Please fill up the form</p>
    <hr>

    <div id="form_messages"></div>
    <form id="sign_up_form" method="post">

        <label for="firstname"><b>Firstname</b></label>
        <input type="text" placeholder="Enter firstname" name="first_name" id="first_name"
               value=""/>

        <label for="lastname"><b>Lastname</b></label>
        <input type="text" placeholder="Enter lastname" name="last_name" id="last_name"
               value=""/>

        <label for="email"><b>Email</b></label>
        <input type="email" placeholder="Enter Email" name="email" value="" id="email"/>


        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="psw" value="" id="psw"/>

        <label for="psw"><b>Confirm Password</b></label>
        <input type="password" placeholder="Re-enter Password" name="psw_confirm" value="" id="psw_confirm"/>

        <button type="submit">Register</button>
    </form>
</div>
<script src="js/main.js"></script>
</body>
</html>