<?php
//starting the session for a user
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

//when the user wants to logout it will destroy the session
if (isset($_GET['logout']) && intval($_GET['logout']) == 1){
    session_destroy();
    header("location:../index.php");
}
//including the header file
include 'header.php';
?>

<body>

<div class="container">
    <div class="row">
        <h3>Dashboard Admin Panel</h3>
        <a type="submit" href="index.php?logout=1" class="btn btn-outline-danger ml-auto">Logout</a>
    </div>
    <!--     navigation for different pages-->
    <nav class="mt-5">
        <a type="button" class="btn btn-primary btn-lg btn-block" href="../index.php">Homepage</a>
        <a type="button" class="btn btn-secondary btn-lg btn-block" href="category.php">Category</a>
        <a type="button" class="btn btn-primary btn-lg btn-block" href="admin_post.php">Posts</a>
        <a type="button" class="btn btn-secondary btn-lg btn-block" href="user_profile.php">Profile</a>
    </nav>

</div>
<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>