<?php
session_start();

require_once("config.php");
include 'header.php';


?>



<!DOCTYPE html>
<html>

<head>
    <title>Login</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
<div class="container">
    <h2>Login</h2>
    <p>Please login to access the Admin panel...</p>
    <hr>

    <div id="form_messages"></div>
    <form action="login.php" method="post" id="login_form">

        <label for="email"><b>Email</b></label>
        <input type="email" placeholder="Enter Email" name="email" id="email" value="" />

        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="psw" id="psw" value=""/>


        <button type="submit" name="user_login" value="1">Login</button>
    </form>
</div>

<script src="js/main.js"></script>
</body>
</html>