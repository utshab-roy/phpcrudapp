<?php
/**
 * this function will delete category of that respective category id
 * @param $category_id
 */
function delete_category($category_id){
    global $conn_oop;
    if ($category_id > 0){
        $sql = "DELETE FROM category WHERE id= $category_id";
        if ($conn_oop->query($sql) === TRUE) {
            //echo 'id number '.$var.' has been deleted';
        }else{
            var_dump($conn_oop->error);
        }
    }
}

/**
 * this function will delete the blog post of the post id
 * @param $blog_id
 */
function delete_post($blog_id){
    global $conn_oop;

    if ($blog_id > 0) {
        //now do the delete operation
        $sql = "DELETE FROM posts WHERE id= $blog_id";
        if ($conn_oop->query($sql) === TRUE) {
            //echo 'id number '.$var.' has been deleted';
        }else{
            var_dump($conn_oop->error);
        }
    }
}

/**
 * inserting new category into the category table
 * @param $title
 */
function insert_category($title){
    global $conn_oop;
    //inserting the category name
    $sql = "INSERT INTO category (name) VALUES ('$title')";
    if ($conn_oop->query($sql) === TRUE){
        echo '<div class="alert alert-primary" role="alert">';
        echo "New category added successfully";
        echo '</div>';
    }
    else {
        var_dump($conn_oop->error);
    }
}

/**
 * this function will update the category name
 * @param $title
 * @param $category_id
 * @return array
 */
function update_category($title, $category_id){
    global $conn_oop;

    //updating category name
    $sql = "UPDATE category SET name='$title' WHERE id='$category_id'";
    if ($conn_oop->query($sql) === TRUE){
        echo "</br>";
        $sql = "SELECT * FROM category WHERE id=$category_id";
        $result = $conn_oop->query($sql);
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            return $row;
        }
    }
    else {
        var_dump($conn_oop->error);
    }
    return array();
}

/**
 * this will show that category which need to change
 * @param $category_id
 * @return array
 */
function show_category_by_id($category_id){
    global $conn_oop;
    $sql = "SELECT * FROM category WHERE id=$category_id";
    $result = $conn_oop->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row;
    }else{
        var_dump($conn_oop->error);
    }
    return array();
}

/**
 * this function will save the post into database according to it's category
 * it will also update the post-category table
 * @param $title
 * @param $content
 * @param $category_id
 */
function add_new_post($title, $content, $category_id){
    global  $conn_oop;
//inserting post into the database
    $sql = "INSERT INTO posts (title, content) VALUES ('$title', '$content')";
    if ($conn_oop->query($sql) === TRUE) {
//getting the last post so that it can add the category to that post
        $sql = "SELECT * FROM posts ORDER BY id DESC LIMIT 1";
        $result = $conn_oop->query($sql);
        $row = $result->fetch_assoc();
        if ($result->num_rows > 0) {
            $post_id =  $row['id'];
            foreach ($category_id as $cat_id):
                //for each category the post will have a value on the post_category table (RELATIONAL TABLE)
                $sql = "INSERT INTO post_category (post_id, category_id) VALUES ('$post_id' , '$cat_id')";
                if ($conn_oop->query($sql) === TRUE){

                }
            endforeach;
            echo '<div class="alert alert-primary" role="alert">';
            echo "Post saved into database successfully";
            echo '</div>';
        }
    } else {
        $general_error = $conn_oop->error;
        echo $general_error;
    }
}

/**
 * this function will update the post
 * @param $title
 * @param $content
 * @param $blog_id
 * @return array
 */
function update_post($title, $content, $blog_id){
    global $conn_oop;

    $sql = "UPDATE posts SET title='$title', content='$content' WHERE id=$blog_id";
    if ($conn_oop->query($sql) === TRUE) {
        echo '<div class="alert alert-success" role="alert">';
        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>';
        echo "Post saved into database successfully";
        echo '</div>';

        $sql = "SELECT * FROM posts WHERE id=$blog_id";
        $result = $conn_oop->query($sql);
        if ($result->num_rows > 0){
            $row = $result->fetch_assoc();
            return $row;
        }
    }
    else {
        $general_error = $conn_oop->error;
        var_dump($general_error);
    }
    return array();
}

/**
 * this function will show the updated post
 * @param $blog_id
 * @return array
 */
function show_post($blog_id){
    global $conn_oop;

    $sql = "SELECT * FROM posts WHERE id=$blog_id";
    $result = $conn_oop->query($sql);
    if ($result->num_rows > 0){
        $row = $result->fetch_assoc();
        return $row;
    }
    return array();
}

/**
 * this function returns the user profile info
 * @param $user_id
 * @return array
 */
function user_profile($user_id){
    global $conn_oop;

    $sql = "SELECT * FROM users WHERE id=$user_id";
    $result = $conn_oop->query($sql);
    $row = $result->fetch_assoc();
    return $row;
}

/**
 * @return bool|mysqli_result
 */
function get_all_country_name(){
    global $conn_oop;

    $sql_for_country = "SELECT * FROM apps_countries";
    $result = $conn_oop->query($sql_for_country);
    $result->fetch_assoc();
    return $result;
}

function profile_pic_upload(){

}