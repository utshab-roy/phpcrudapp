<?php
session_start();
require_once("config.php");


//necessary array for the messages
$output = array();
$output['validation'] = 1;
$output['success_messages'] = array();
$output['validation_messages'] = array();
$output['error_messages'] = array();

$validation_messages  = array();
$success_messages  = array();
$error_messages  = array();


$email = $password  = '';
$email_error        = '';
$password_error     = '';
$general_error      = '';

$formValid = true;
$fieldEmailValid = true;
$fieldPasswordValid = true;



if (isset($_POST['email'])){
    $email = $_POST['email'];
    if(empty($email)){
        $validation_messages['email'] =  'Email is empty, give your email';
        $output['validation'] = 0;

    }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $validation_messages['email'] =  'Invalid email';
        $output['validation'] = 0;
    }
}

if (isset($_POST['psw'])){
    $password = $_POST['psw'];
    if(empty($password)){
        $validation_messages['psw'] =  'Password can not be empty';
        $output['validation'] = 0;
    }
}

$output['validation_messages'] = $validation_messages;

if(intval($output['validation']) == 1){
    //this function will check for the validation of the user login
    $success_messages = login_validation($email, $password);
    $output['success_messages'] = $success_messages;
    echo json_encode($output);
}
else{
    //form is not valid
    echo json_encode($output);
}







die();