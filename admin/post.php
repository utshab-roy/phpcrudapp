<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

require_once("config_admin.php");
include 'header.php';

$title = $content = '';

$form_valid = true;

if (isset($_POST['post_submit']) && intval($_POST['post_submit']) == 1){

    if (isset($_POST['category'])){
        $category_id = $_POST['category'];
        if (empty($category_id)){
            $form_valid = false;
        }
    }else{
        $category_id[] = "9";
    }

    if (isset($_POST['title'])){
        $title = $_POST['title'];
        if (empty($title)){
            $form_valid = false;
        }
    }

    if (isset($_POST['content'])){
        $content = $_POST['content'];
        if (empty($content)){
            $form_valid = false;
        }
    }


    if ($form_valid){
        /*
         * this function will save the post into database according to it's category
         * it will also update the post-category table
         */
        add_new_post($title, $content, $category_id);
    }
}

?>


<body>
<form action="post.php" method="post">
    <div class="container">
        <div>
            <label for="title"><h1>Title: </h1></label>
            <input type="text" class="form-control"  placeholder="Enter title" name="title">
        </div>


        <div class="">
            <h3>Select category:</h3>

            <?php
            $sql = "SELECT * FROM category";
            $result = $conn_oop->query($sql);
            if ($result->num_rows > 0):
                while ($row = $result->fetch_assoc()): ?>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="category[]" value="<?php echo $row['id']; ?>">
                            <?php echo $row['name']; ?>
                        </label>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>


        <div>
            <label for="content"><h3>Content: </h3></label>
            <textarea class="form-control" placeholder="Write something about the post" rows="5" name="content"></textarea>
        </div>

        <button type="submit" class="btn btn-primary" name="post_submit" value="1">Post</button>
        <a type="button" class="btn btn-dark float-right" href="index.php">Home</a>
    </div>
</form>

<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>

