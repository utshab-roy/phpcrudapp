<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

include 'header.php';
require_once("config_admin.php");

$title = '';
$formValid = true;

if (isset($_POST['submit']) && intval($_POST['submit'] == 1)){

    if (isset($_POST['title'])){

        $title = $_POST['title'];

        if(empty($title)){
            $formValid = false;
            $email_error = 'Title is empty';
        }
    }
    if ($formValid){
//        inserting the category into the category table
        insert_category($title);
    }

}

?>

<body>
<div class="container">
    <form action="category_new.php" method="post">
        <div class="form-group">
            <label for="title"><h1>Title</h1></label>
            <input type="text" class="form-control" name="title" placeholder="Enter the title">
        </div>
        <button class="btn btn-primary" type="submit" name="submit" value="1">Submit</button>
        <a type="button" class="btn btn-dark float-right" href="index.php">Home</a>
    </form>
</div>

<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>