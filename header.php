<!DOCTYPE html>
<html lang = "en">

<head>
    <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE = edge">
    <meta name = "viewport" content = "width = device-width, initial-scale = 1">

    <title>Homepage Blog</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

<!--    font font-awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

<!--    My css stylesheet-->
<!--    <link rel="stylesheet" href="css/style.css">-->
    <style>
        .container{
            /*background-color:#c6c8ca ;*/
            margin-top: 10px;
        }

        .nav{
            font-size: 20px;
        }
    </style>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery.validate.js"></script>
</head>