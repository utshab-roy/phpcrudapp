<?php
session_start();
include 'header.php';
include 'admin/paginator.class.php';
require_once("config.php");
require_once ("admin/config_admin.php");

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";

//default order_by set to posts.id for the homepage
$order_by = isset($_GET['order_by']) ? $_GET['order_by'] : 'posts.id';


?>

<body>
<div class="container">
    <div class="row">

        <div class="col-4">
            <h1>Personal Blog</h1>
        </div>

        <div class="col-8">
            <div class="container">
                <div class="row">
                    <div class="col-12">

                    </div>

                    <div class="col-12">
                        <nav class="nav justify-content-end">
                            <a class="nav-link active" href="index.php">Home</a>
                            <a class="nav-link" href="admin/index.php">Admin</a>
                            <a class="nav-link" href="register.php">Sign Up</a>
                            <a class="nav-link" href="login.php">Login</a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container ">
    <div class="row">
        <div class="col-8">
            <h3 class="text-center">Main Blog</h3>
            <p class="text-center">Here all the blog post will be shown.</p>
            <!--            search bar-->
            <div class="searchbox_wrap">
                <form action="index.php" method="get">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search"/>
                        <div class="input-group-btn">
                            <button class="btn btn-primary" type="submit" name="submit" value="search">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--            search bar end-->

            <h3>All the posts</h3>

            <!--                //hear should be the loop for retrieving-->
            <?php
            //get all blog posts
            //$sql = "SELECT * FROM posts";
            $part_of_sql = "";
            if (isset($_GET['submit']) && isset($_GET['search'])){
                if (isset($_GET['search'])) {
                    $search = $_GET['search'];
                    $part_of_sql = " HAVING posts.title LIKE '%" . $search . "%' OR posts.content LIKE '%" . $search . "%'";
                }
            }
            //show all the posts
            $main_sql = "SELECT DISTINCT posts.*, GROUP_CONCAT(post_category.category_id) AS cat_id, GROUP_CONCAT(category.name) AS categories FROM posts LEFT JOIN post_category ON post_category.post_id = posts.id LEFT JOIN category on category.id = post_category.category_id GROUP BY posts.id";

            $sql = "$main_sql" . "$part_of_sql";
            $result = $conn_oop->query($sql);
            $count = $result->num_rows;
            //******************************
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", "$username", "$password");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            if (!isset($_GET['search'])) {
                $num_rows = $conn->query('SELECT COUNT(*) FROM posts')->fetchColumn();
            }else{
                $num_rows = $count;
            }

            $pages = new Paginator($num_rows, 9, array(6, 3, 6, 9, 12, 25, 50, 'All'));
            //        echo $pages->display_pages();
            $stmt = $conn->prepare($sql . " ORDER BY $order_by DESC LIMIT :start,:end");
            $stmt->bindParam(':start', $pages->limit_start, PDO::PARAM_INT);
            $stmt->bindParam(':end', $pages->limit_end, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchAll();
            //******************************

            foreach ($result as $row) :
                $blog_id = intval($row['id']);
                ?>
                <div class="col-12">
                    <h4><?php echo $row['title']; ?></h4>
                    <p class="text-justify"><?php echo $row['content']; ?></p>
                    <p class="text-right"><i class="far fa-clock"> Created at: <?php echo $row['created_at']; ?></i></p>
                    <p>category:
                        <?php
                        echo $row['categories'];
                        ?>
                    </p>
                    <br/>
                </div>
                <?php
            endforeach;
            ?>

            <div class='container text-lg-center'><span ><?php echo $pages->display_jump_menu(); ?><?php echo $pages->display_items_per_page(); ?></span></div>
            <div class='container text-lg-center '><?php echo $pages->display_pages(); ?></div>


<!--            <a href="post.php">-->
<!--                <button type="button" class="btn btn-primary mt-2 mb-5">Add new post</button>-->
<!--            </a>-->
        </div>


        <div class="col-4">
            <h3>Sidebar</h3>
            <p>Some info and other element will be here.</p>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h4>Category</h4>
                        <p>Label of some category</p>
                    </div>
                    <div class="col-12">
                        <h4>Side widget</h4>
                        <p>may be a calender or interactive thing.</p>
                    </div>
                    <div class="col-12">
                        <h4>Sort By</h4>
                        <ul>
                            <li><a href="index.php?order_by=<?php $_GET['order_by'] = 'posts.title'; echo  $order_by = $_GET['order_by'];?>">Title</a></li>
                            <li><a href="">Category</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap some required files-->
<script src="js/bootstrap.bundle.js"></script>
<script src="js/bootstrap.js"></script>
</body>
</html>