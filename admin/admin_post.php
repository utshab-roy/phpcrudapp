<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

require_once("config_admin.php");
include 'header.php';
include 'paginator.class.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";

$current_order = isset($_GET['order']) ? $_GET['order']: 'desc';
$order_by = isset($_GET['order_by']) ? $_GET['order_by']: 'posts.id';
$search = isset($_GET['search']) ? $_GET['search']: '';


if (isset($_GET['action']) && (intval($_GET['action']) == 1)) {

    $type = isset($_GET['type']) ? $_GET['type'] : 'none';

    switch ($type) {
        case 'delete':
            //blog delete by blog 'id'
            $blog_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
            delete_post($blog_id);
            break;
        case 'pagination':
            //perform pagination in index

            break;

        default:
    }
}

$part_of_sql = "";
if (isset($_GET['submit']) && isset($_GET['search'])){
    if (isset($_GET['search'])) {
        $search = $_GET['search'];
//        echo $search;
        $part_of_sql = " HAVING title LIKE '%" . $search . "%' OR content LIKE '%" . $search . "%'";
    }
}

?>

<body>
<div class="container">
    <div class="row">
        <div class="col-4">
            <h1>Personal Blog</h1>
        </div>
        <div class="col-8">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                    </div>
                    <div class="col-12">
                        <nav class="nav justify-content-end">
                            <a class="nav-link active" href="../index.php">Home</a>
                            <a class="nav-link" href="index.php">Admin</a>
                            <a class="nav-link" href="../register.php">Sign Up</a>
                            <a class="nav-link" href="../login.php">Login</a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container ">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center">Admin panel</h3>
            <p class="text-center">Here we will manage all the posts.</p>

            <!--            search bar-->
            <div class="searchbox_wrap">
                <form action="admin_post.php" method="get">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search"/>
                        <div class="input-group-btn">
                            <button class="btn btn-primary" type="submit" name="submit" value="search">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--            search bar end-->

            <h3>All the posts</h3>

            <!--                //hear should be the loop for retrieving-->
            <?php
            //get all blog posts
            $main_sql = "SELECT DISTINCT posts.*, GROUP_CONCAT(post_category.category_id) AS cat_id, GROUP_CONCAT(category.name) AS categories FROM posts LEFT JOIN post_category ON post_category.post_id = posts.id LEFT JOIN category on category.id = post_category.category_id GROUP BY posts.id";
            $sql = "$main_sql" . "$part_of_sql";
            $result = $conn_oop->query($sql);
            $count = $result->num_rows;

            //******************************
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", "$username", "$password");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            if (isset($_GET['search'])) {
                $num_rows = $count;
            }else{
                $num_rows = $conn->query('SELECT COUNT(*) FROM posts')->fetchColumn();
            }

            $pages = new Paginator($num_rows, 9, array(6, 3, 6, 9, 12, 25, 50, 100, 250, 'All'));
            //        echo $pages->display_pages();

            $stmt = $conn->prepare($sql . " ORDER BY $order_by $current_order LIMIT :start,:end");
            $stmt->bindParam(':start', $pages->limit_start, PDO::PARAM_INT);
            $stmt->bindParam(':end', $pages->limit_end, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchAll();
            //******************************

            $args = array();

            $args_temp = explode("&",$_SERVER["QUERY_STRING"]);

            if (!$_SERVER["QUERY_STRING"] == ""){
                foreach ($args_temp as $val) {
                    $single_value = explode("=", $val);
                    $args[$single_value[0]] = $single_value[1];
                }
            }

            $marge = array();
            foreach ($args as $key=>$val){
                $marge[$key] =  $key . "=" . $val;
            }
//            echo "<pre>";
//            print_r($marge);
//            echo "</pre>";

            function url_builder($order, $order_by, $search=''){
                $marge['search'] = 'search=' . $search;
                $marge['submit'] = 'submit=search';
                $marge['order'] = ($order === 'desc') ? 'order=asc' : 'order=desc';
                $marge['order_by'] ='order_by=' . $order_by;
                $url = implode("&", $marge);
                echo $url;
            }
//            $id_args = $args;
//            $id_args['order'] = $current_order;
//            $id_args['order_by'] = 'id';


            ?>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col"><a href="admin_post.php?<?php url_builder($current_order, 'posts.id', $search);?>">ID</a></th>
                    <th scope="col"><a href="admin_post.php?<?php url_builder($current_order, 'posts.title', $search);?>">Title</a></th>
                    <th scope="col"><a href="admin_post.php?<?php url_builder($current_order, 'category.name', $search);?>">Categories</a></th>
                    <th scope="col">Action</th>
                </tr>
                </thead>

                <?php
                foreach ($result as $row):
                    $post_id = $row['id'];
                    ?>
                    <tr>
                        <th scope="row"><?php echo $row['id']; ?></th>
                        <td><?php echo $row['title'] ?></td>
                        <td><?php
                            $cat = $row['categories'];
                            $category_name_array = explode(',', $cat);
                            $cat_id = $row['cat_id'];
                            $cat_id_array = explode(',', $cat_id);

                            for ($i = 0; $i < sizeof($cat_id_array); $i++) :?>
                                <a href="update_catagory.php?id=<?php echo $cat_id_array["$i"]; ?>&type=edit&action=1"><?php echo $category_name_array["$i"]; ?></a>
                                <?php
                            endfor;
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-primary btn-sm"
                               href="update_post.php?id=<?php echo $row['0'] ?>&type=edit"> Edit</a>
                            <a class="btn btn-danger btn-sm"
                               href="index.php?id=<?php echo $row['0']; ?>&type=delete&action=1">Delete</a>
                        </td>
                    </tr>
                <?php
                endforeach;
                ?>
            </table>

            <div class='container text-lg-center'><span ><?php echo $pages->display_jump_menu(); ?><?php echo $pages->display_items_per_page(); ?></span></div>
            <div class='container text-lg-center '><?php echo $pages->display_pages(); ?></div>

            <a class="btn btn-primary mt-2 mb-5" href="post.php" role="button">Add NEW post</a>
        </div>
    </div>
</div>


<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>