<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

require_once("config_admin.php");
include 'header.php';

$title = $content = $ID = '';
$general_error = '';

$form_valid = true;

$blog_id = isset($_GET['id'])? intval($_GET['id']): 0;

if (isset($_POST['post_submit']) && intval($_POST['post_submit']) == 1){
    $blog_id = isset($_POST['id'])? intval($_POST['id']): 0;

    if (isset($_POST['title'])){
        $title = $_POST['title'];
        if (empty($title)){
            $form_valid = false;
        }
    }

    if (isset($_POST['content'])){
        $content = $_POST['content'];
        if (empty($content)){
            $form_valid = false;
        }
    }

    if ($blog_id == 0){
        $form_valid = false;
    }

    if ($form_valid){
        $row = update_post($title, $content, $blog_id);
    }
}

$row = show_post($blog_id);

?>

<body>
<form action="update_post.php" method="post">
    <div class="container">

        <div>
            <label for="title"><h1>Title: </h1></label>
            <input type="text" class="form-control" name="title" value="<?= $row['title']; ?>">
        </div>

        <div>
            <label for="content"><h3>content: </h3></label>
            <textarea class="form-control" rows="5" name="content"><?= $row['content']; ?></textarea>
        </div>
        <input type="hidden" name="id" value="<?php echo $blog_id ?>"/>

        <button type="submit" class="btn btn-primary" name="post_submit" value="1">Post</button>

        <a href="index.php" class="btn btn-dark float-right">Home</a>
    </div>
</form>

<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>