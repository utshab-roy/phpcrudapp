<?php
session_start();
//if the user is unable to login then redirect to the login page
if(!$_SESSION['logged_in']) {
    header("location:../login.php");
    die();
}

require_once("config_admin.php");
include 'header.php';
$user_id = $_SESSION['id'];

//returns back the user information
$row = user_profile($user_id);


?>

<body>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-8">
            <h1 class="text-center">Profile</h1>
            <img src="uploads/<?php echo $row['firstname'] .'.'. 'jpg'?>" />

            <div class="row">
                <h3 class="float-lg-right">Hello, <?php echo $row['firstname'] . " " . $row['lastname'] ?></h3>
                <a class="btn btn-primary ml-auto" href="edit_profile.php" role="button">Edit Profile</a>
                <a class="btn btn-secondary ml-auto" href="edit_profile_chosen.php" role="button">Edit Chosen</a>
                <a class="btn btn-warning ml-auto" href="edit_profile_select2.php" role="button">Edit Select2</a>
            </div>

            <div class="row">
                <div class="col-4">
                    <p>Firstname</p>
                </div>
                <div class="col-8">
                    <p><?php echo $row['firstname']; ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <p>Lastname</p>
                </div>
                <div class="col-8">
                    <p><?php echo $row['lastname']; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <p>Email</p>
                </div>
                <div class="col-8">
                    <p><?php echo $row['email']; ?></p>
                </div>
            </div>

            <!--         this part is for less important information-->

            <?php
            if (!($row['info'] === "")):
                $address = unserialize($row['info']);
                foreach ($address as $key => $val):
                    if ($val === "") continue; ?>
                    <div class="row">
                        <div class="col-4">
                            <p><?php echo $key; ?></p>
                        </div>
                        <div class="col-8">
                            <p><?php echo $val; ?></p>
                        </div>
                    </div>
                    <?php
                endforeach;
            endif;
            ?>
            <a type="button" class="btn btn-secondary btn-lg btn-block" href="index.php">Admin Panel</a>
        </div>
    </div>
</div>


<!-- Bootstrap some required files-->
<script src="../js/bootstrap.bundle.js"></script>
<script src="../js/bootstrap.js"></script>
</body>
</html>